# 龙骧 - Ryujo #

This is a solution of voice call between mobile phone users and servers.

# Work Flow ##
User --call--> Middle Server --retransmit--> Server

Middler Server is on the public network, while users or servers may be on a private network.

# Open Source Libraries #
1. Speex      Copyright ©2002-2007 Jean-Marc Valin/Xiph.org Foundation (GPL License)
2. JThread    Copyright © 2006-2014 Jori Liesenborgs
3. JRTP       Copyright © 2006-2014 Jori Liesenborgs