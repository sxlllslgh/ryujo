#include "ryujo.h"

JNIEXPORT void JNICALL Java_sxlllslgh_ryujo_Communicator_StartRecord(JNIEnv *env, jobject obj, jint server_ip, jint server_port, jint portbase, jint compression)
{
    /* Judge if recording has started. */
    if(is_start_record)
    {
        return;
    }
    int status = speex_success_result;
    /* Create send session. */
    RTPUDPv4TransmissionParams transparams;
    RTPSessionParams sessparams;
    sessparams.SetOwnTimestampUnit(1.0 / 1000.0);
    transparams.SetPortbase(portbase);
    int temp_compression = compression;
    SendRTPSession sess(&temp_compression);
    status = sess.Create(sessparams, &transparams);
    assert(speex_success_result == status);
    /* Add destination address into send session. */
    RTPIPv4Address addr(server_ip, server_port);
    sess.AddDestination(addr);
    /* Set attributes of packets to send. */
    sess.SetDefaultPayloadType(0);
    sess.SetDefaultMark(false);
    sess.SetDefaultTimestampIncrement(20);

    short *record_buffer = (short *)malloc(sess.GetEncoderFrameSize() * sizeof(short));

    SLresult result;
    /* Create and initialize engine. */
    SLObjectItf engine_obj = NULL;
    SLEngineItf engine_interface;
    result = slCreateEngine(&engine_obj, 0, NULL, 0, NULL, NULL); // Create SL engine.
    assert(SL_RESULT_SUCCESS == result);
    result = (*engine_obj)->Realize(engine_obj, SL_BOOLEAN_FALSE); // Realize the engine.
    assert(SL_RESULT_SUCCESS == result);
    result = (*engine_obj)->GetInterface(engine_obj, SL_IID_ENGINE, &engine_interface); // Get the engine interface, which is needed in order to create other objects.
    assert(SL_RESULT_SUCCESS == result);

    /* Recorder interfaces. */
    SLObjectItf recorder_obj = NULL;
    SLRecordItf recorder_interface;
    SLAndroidSimpleBufferQueueItf recorder_buffer_queue;

    /* Initialize recorder. */
    SLDataLocator_IODevice loc_dev = { SL_DATALOCATOR_IODEVICE, SL_IODEVICE_AUDIOINPUT, SL_DEFAULTDEVICEID_AUDIOINPUT, NULL };
    SLDataSource audio_src = { &loc_dev, NULL };
    SLDataLocator_AndroidSimpleBufferQueue loc_bq = { SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2 };
    SLDataFormat_PCM format_pcm = { SL_DATAFORMAT_PCM, 1, SL_SAMPLINGRATE_16, SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16, SL_SPEAKER_FRONT_CENTER, SL_BYTEORDER_LITTLEENDIAN };
    SLDataSink audio_sink = { &loc_bq, &format_pcm };

    const SLInterfaceID id[1] = { SL_IID_ANDROIDSIMPLEBUFFERQUEUE };
    const SLboolean req[1] = { SL_BOOLEAN_TRUE };
    result = (*engine_interface)->CreateAudioRecorder(engine_interface, &recorder_obj, &audio_src, &audio_sink, 1, id, req);
    assert(SL_RESULT_SUCCESS == result);
    result = (*recorder_obj)->Realize(recorder_obj, SL_BOOLEAN_FALSE);
    assert(SL_RESULT_SUCCESS == result);
    (*recorder_obj)->GetInterface(recorder_obj, SL_IID_RECORD, &recorder_interface);
    (*recorder_obj)->GetInterface(recorder_obj, SL_IID_ANDROIDSIMPLEBUFFERQUEUE, &recorder_buffer_queue);

    (*recorder_interface)->SetRecordState(recorder_interface, SL_RECORDSTATE_STOPPED);
    (*recorder_buffer_queue)->Clear(recorder_buffer_queue);

    /* Start recording. */
    (*recorder_interface)->SetRecordState(recorder_interface, SL_RECORDSTATE_RECORDING);

    is_start_record = 1;
    while(is_start_record)
    {
        (*recorder_buffer_queue)->Enqueue(recorder_buffer_queue, record_buffer, sess.GetEncoderFrameSize() * sizeof(short));
        sess.SendPacket(record_buffer);
    }

    /* Stop recording. */
    (*recorder_interface)->SetRecordState(recorder_interface, SL_RECORDSTATE_STOPPED);
    (*recorder_obj)->Destroy(recorder_obj);
    free(record_buffer);
    sess.Destroy();
}

JNIEXPORT void JNICALL Java_sxlllslgh_ryujo_Communicator_StopRecord(JNIEnv *env, jobject obj)
{
    is_start_record = 0;
}

JNIEXPORT void JNICALL Java_sxlllslgh_ryujo_Communicator_StartPlay(JNIEnv *env, jobject obj, jint portbase)
{
    if(is_start_play)
    {
        return;
    }
    /* Create receive session. */
    RTPSessionParams sessparams;
    sessparams.SetOwnTimestampUnit(1.0 / 1000.0);
    RTPUDPv4TransmissionParams transparams;
    transparams.SetPortbase(portbase);
    ReceiveRTPSession sess;
    sess.Create(sessparams, &transparams);

    sess.Start();

    is_start_play = 1;
    while(is_start_play)
    {
        RTPTime::Wait(RTPTime(1, 0));
    }

    sess.Destroy();
}

JNIEXPORT void JNICALL Java_sxlllslgh_ryujo_Communicator_StopPlay(JNIEnv *env, jobject obj)
{
    is_start_play = 0;
}