LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE     := libryujo
LOCAL_CFLAGS      =  -DFIXED_POINT -DUSE_KISS_FFT -DEXPORT="" -UHAVE_CONFIG_H
LOCAL_C_INCLUDES := $(LOCAL_PATH)/include/speex $(LOCAL_PATH)/include/jthread $(LOCAL_PATH)/include/jrtp $(LOCAL_PATH)

LOCAL_SRC_FILES  := ryujo.cpp sender.cpp receiver.cpp \
					source/speex/bits.c source/speex/cb_search.c source/speex/exc_10_16_table.c source/speex/exc_10_32_table.c source/speex/exc_20_32_table.c source/speex/exc_5_256_table.c source/speex/exc_5_64_table.c source/speex/exc_8_128_table.c source/speex/filters.c source/speex/gain_table.c source/speex/gain_table_lbr.c source/speex/hexc_10_32_table.c source/speex/hexc_table.c source/speex/high_lsp_tables.c source/speex/kiss_fft.c source/speex/kiss_fftr.c source/speex/lpc.c source/speex/lsp.c source/speex/lsp_tables_nb.c source/speex/ltp.c source/speex/modes.c source/speex/modes_wb.c source/speex/nb_celp.c source/speex/quant_lsp.c source/speex/sb_celp.c source/speex/smallft.c source/speex/speex.c source/speex/speex_callbacks.c source/speex/speex_header.c source/speex/stereo.c source/speex/vbr.c source/speex/vorbis_psy.c source/speex/vq.c source/speex/window.c \
					source/jthread/jthread.cpp source/jthread/jmutex.cpp \
					source/jrtp/rtcpapppacket.cpp source/jrtp/rtcpbyepacket.cpp source/jrtp/rtcpcompoundpacket.cpp source/jrtp/rtcpcompoundpacketbuilder.cpp source/jrtp/rtcppacket.cpp source/jrtp/rtcppacketbuilder.cpp source/jrtp/rtcprrpacket.cpp source/jrtp/rtcpscheduler.cpp source/jrtp/rtcpsdesinfo.cpp source/jrtp/rtcpsdespacket.cpp source/jrtp/rtcpsrpacket.cpp source/jrtp/rtpbyteaddress.cpp source/jrtp/rtpcollisionlist.cpp source/jrtp/rtpdebug.cpp source/jrtp/rtperrors.cpp source/jrtp/rtpexternaltransmitter.cpp source/jrtp/rtpinternalsourcedata.cpp source/jrtp/rtpipv4address.cpp source/jrtp/rtpipv6address.cpp source/jrtp/rtplibraryversion.cpp source/jrtp/rtppacket.cpp source/jrtp/rtppacketbuilder.cpp source/jrtp/rtppollthread.cpp source/jrtp/rtprandom.cpp source/jrtp/rtprandomrand48.cpp source/jrtp/rtprandomrands.cpp source/jrtp/rtprandomurandom.cpp source/jrtp/rtpsession.cpp source/jrtp/rtpsessionparams.cpp source/jrtp/rtpsessionsources.cpp source/jrtp/rtpsourcedata.cpp source/jrtp/rtpsources.cpp source/jrtp/rtptimeutilities.cpp source/jrtp/rtpudpv4transmitter.cpp source/jrtp/rtpudpv6transmitter.cpp
LOCAL_LDLIBS     += -lOpenSLES
LOCAL_LDLIBS     += -landroid
LOCAL_LDLIBS     += -lm -llog

include $(BUILD_SHARED_LIBRARY)
