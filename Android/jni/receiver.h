/**************************************************************************

Copyright: Ryujo

Author: Ice Cream Waffle(sxlllslgh)

Date: 2016-04-19

Description: The header file of receiver.

**************************************************************************/
/* Custom configuration. */
#include <config.h>

/* Standard C++ headers. */
#include <cassert>

/* Speex library header.  */
#include <speex.h>

/* JRTP library headers.  */
#include <rtpsession.h>
#include <rtppacket.h>
#include <rtpudpv4transmitter.h>
#include <rtpipv4address.h>
#include <rtpsessionparams.h>
#include <rtperrors.h>

/* OpenSL ES headers. */
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
#include <SLES/OpenSLES_AndroidConfiguration.h>

/* Use JRTP library namespace. */
using namespace jrtplib;

/*
 * The custom class of receiver.
 * The way to use is according by JRTP offical example 4.
 * There must be JThread library to use it.
 */
class ReceiveRTPSession : public RTPSession
{
public:
    ReceiveRTPSession();
    ~ReceiveRTPSession();
    void Start();
    void Destroy();
private:
    /* Speex decoder variables. */
    SpeexBits decoder_bits; // Speex decoder Speex Bits.
    int decoder_frame_size; // Speex decoder frame size.
    void *decoder_state;    // Speex decoder state.

    /* The play buffer variables. */
    short **play_buffer;   // The play buffer, which is a two-dimension array.
    int play_buffer_index; // The index to show which buffer would to be use.

    /* OpenSL ES interfaces. */
    SLObjectItf engine_obj;
    SLEngineItf engine_interface;

    SLObjectItf output_mix_obj;

    SLObjectItf play_obj;
    SLPlayItf play_interface;
    SLAndroidSimpleBufferQueueItf play_buffer_queue;
    SLEffectSendItf play_effect_send;
    SLVolumeItf play_volume;
protected:
    /* Wait to get each RTP packet. */
    void OnPollThreadStep();
    /* Process packet received. */
    void ProcessRTPPacket(const RTPPacket &rtppack);
};