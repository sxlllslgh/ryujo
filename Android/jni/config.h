/**************************************************************************

Copyright: Ryujo

Author: Ice Cream Waffle(sxlllslgh)

Date: 2016-04-19

Description: The global configuration.

**************************************************************************/
#ifndef _RYUJO_CONFIG_H_
#define _RYUJO_CONFIG_H_

/* Define the count of play buffers, which were used to store audio received temporarily. */
const int play_buffer_count = 50;

/* Define success result codes. */
const int speex_success_result = 0;
const int jrtp_success_result = 0;

#endif