/**************************************************************************

Copyright: Ryujo

Author: Ice Cream Waffle(sxlllslgh)

Date: 2016-04-19

Description: The header file of sender.

**************************************************************************/
/* Custom configuration. */
#include <config.h>

/* Standard C++ headers. */
#include <cassert>

/* Speex library header.  */
#include <speex.h>

/* JRTP library headers.  */
#include <rtpsession.h>
#include <rtppacket.h>
#include <rtpudpv4transmitter.h>
#include <rtpipv4address.h>
#include <rtpsessionparams.h>
#include <rtperrors.h>

/* OpenSL ES headers. */
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>

/* Use JRTP library namespace. */
using namespace jrtplib;

/*
 * The custom class of receiver.
 * The way to use is according by JRTP offical example 4.
 * There must be JThread library to use it.
 */
class SendRTPSession : public RTPSession
{
public:
	SendRTPSession(int *compression);
	~SendRTPSession();
	int GetEncoderFrameSize();
	void SendPacket(short *record_buffer);
private:
    /* Speex encoder variables. */
    SpeexBits encoder_bits; // Speex encoder Speex Bits.
    int encoder_frame_size; // Speex encoder frame size.
    void *encoder_state;    // Speex encoder state.
    int encode_size;

    /* Audio data buffers. */
    short * pcm_buffer;
    char * output_buffer;
};