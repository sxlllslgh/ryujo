/**************************************************************************

Copyright: Ryujo

Author: Ice Cream Waffle(sxlllslgh)

Date: 2016-04-19

Description: The header file of jni communicator.

**************************************************************************/

/* JNI header. */
#include <jni.h>

/* Standard C++ headers. */
#include <cstdlib>
#include <cassert>

/* UNIX system headers. */
#include <unistd.h>
#include <sys/types.h>

/* OpenSL ES headers. */
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>

/* Android headers. */
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <android/log.h>

/* Custom configuration. */
#include "config.h"

/* Receiver header. */
#include "receiver.h"
#include "sender.h"

static int is_start_record = 0, is_start_play = 0;

extern "C" JNIEXPORT void JNICALL Java_sxlllslgh_ryujo_Communicator_StartRecord(JNIEnv *env, jobject obj);

extern "C" JNIEXPORT void JNICALL Java_sxlllslgh_ryujo_Communicator_StopRecord(JNIEnv *env, jobject obj);

extern "C" JNIEXPORT void JNICALL Java_sxlllslgh_ryujo_Communicator_StartPlay(JNIEnv *env, jobject obj);

extern "C" JNIEXPORT void JNICALL Java_sxlllslgh_ryujo_Communicator_StopPlay(JNIEnv *env, jobject obj);