#include "sender.h"

SendRTPSession::SendRTPSession(int *compression)
{
	encoder_frame_size = 0;
	speex_bits_init(&encoder_bits);
	encoder_state = speex_encoder_init(&speex_wb_mode);
	speex_encoder_ctl(encoder_state, SPEEX_SET_QUALITY, &compression);
	speex_encoder_ctl(encoder_state, SPEEX_GET_FRAME_SIZE, &encoder_frame_size);

	pcm_buffer = (short *)malloc(encoder_frame_size * sizeof(short));
	output_buffer = (char *)malloc(encoder_frame_size * sizeof(char));
}

SendRTPSession::~SendRTPSession()
{
	speex_bits_destroy(&encoder_bits);
    speex_encoder_destroy(encoder_state);
	free(pcm_buffer);
	free(output_buffer);
}

int SendRTPSession::GetEncoderFrameSize()
{
	return encoder_frame_size;
}

void SendRTPSession::SendPacket(short *record_buffer)
{
	speex_bits_reset(&encoder_bits);
	memcpy(pcm_buffer, record_buffer, encoder_frame_size * sizeof(short));
	speex_encode_int(encoder_state, pcm_buffer, &encoder_bits);
	encode_size = speex_bits_write(&encoder_bits, output_buffer, encoder_frame_size);
	RTPSession::SendPacket(output_buffer, encode_size);
}