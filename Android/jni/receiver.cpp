/**************************************************************************

Copyright: Ryujo

Author: Ice Cream Waffle(sxlllslgh)

Date: 2016-04-19

Description: The source file of receiver.

**************************************************************************/
#include "receiver.h"

ReceiveRTPSession::ReceiveRTPSession()
{
    int status = 0;
    /* Initialize decoder Speex Bits. */
    speex_bits_init(&decoder_bits);
    /*
     * Use wideband mode to initialize decoder.
     * Parameter "speex_nb_mode" means narrowband mode.
     * Parameter "speex_uwb_mode" means ultra-wideband mode.
     */
    decoder_state = speex_decoder_init(&speex_wb_mode);
    /* Set Speex decoder frame size. */
    status = speex_decoder_ctl(decoder_state, SPEEX_GET_FRAME_SIZE, &decoder_frame_size);
    assert(speex_success_result == status);
    /* Allocate memory to each play buffer. */
    for(play_buffer_index = 0; play_buffer_index < play_buffer_count; play_buffer_index++)
    {
        play_buffer[play_buffer_index] = (short *)malloc(decoder_frame_size * sizeof(short));
    }
    /* Initialize play_buffer_index. */
    play_buffer_index = 0;

    SLresult result;
    /* Create and initialize engine. */
    result = slCreateEngine(&engine_obj, 0, NULL, 0, NULL, NULL); // Create SL engine.
    assert(SL_RESULT_SUCCESS == result);
    result = (*engine_obj)->Realize(engine_obj, SL_BOOLEAN_FALSE); // Realize the engine.
    assert(SL_RESULT_SUCCESS == result);
    result = (*engine_obj)->GetInterface(engine_obj, SL_IID_ENGINE, &engine_interface); // Get the engine interface, which is needed in order to create other objects.
    assert(SL_RESULT_SUCCESS == result);

    /* Create and initialize mix */
    const SLInterfaceID id_mix[1] = { SL_IID_ENVIRONMENTALREVERB }; // Create output mix, with environmental reverb specified as a non-required interface.
    const SLboolean req_mix[1] = { SL_BOOLEAN_FALSE };
    result = (*engine_interface)->CreateOutputMix(engine_interface, &output_mix_obj, 1, id_mix, req_mix);
    assert(SL_RESULT_SUCCESS == result);
    result = (*output_mix_obj)->Realize(output_mix_obj, SL_BOOLEAN_FALSE);
    assert(SL_RESULT_SUCCESS == result);

    /* Create and initialize player. */
    SLDataLocator_AndroidSimpleBufferQueue loc_bq = { SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2 };
    SLDataFormat_PCM format_pcm = { SL_DATAFORMAT_PCM, 1, SL_SAMPLINGRATE_16, SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16, SL_SPEAKER_FRONT_CENTER, SL_BYTEORDER_LITTLEENDIAN };
    SLDataSource audio_src = { &loc_bq, &format_pcm };
    SLDataLocator_OutputMix loc_outmix = { SL_DATALOCATOR_OUTPUTMIX, output_mix_obj };
    SLDataSink audio_sink = { &loc_outmix, NULL };
    const SLInterfaceID id_play[4] = { SL_IID_ANDROIDCONFIGURATION, SL_IID_BUFFERQUEUE, SL_IID_EFFECTSEND, SL_IID_VOLUME };
    const SLboolean req_play[4] = { SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE };
    result = (*engine_interface)->CreateAudioPlayer(engine_interface, &play_obj, &audio_src, &audio_sink, 4, id_play, req_play);
    assert(SL_RESULT_SUCCESS == result);
    /* Make ear speaker the output device. */
    SLAndroidConfigurationItf player_config;
    result = (*play_obj)->GetInterface(play_obj, SL_IID_ANDROIDCONFIGURATION, &player_config);
    assert(SL_RESULT_SUCCESS == result);
    SLint32 stream_type = SL_ANDROID_STREAM_VOICE;
    result = (*player_config)->SetConfiguration(player_config, SL_ANDROID_KEY_STREAM_TYPE, &stream_type, sizeof(SLint32));
    assert(SL_RESULT_SUCCESS == result);
    /* Realize object and get interfaces. */
    result = (*play_obj)->Realize(play_obj, SL_BOOLEAN_FALSE);
    assert(SL_RESULT_SUCCESS == result);
    result = (*play_obj)->GetInterface(play_obj, SL_IID_PLAY, &play_interface);
    assert(SL_RESULT_SUCCESS == result);
    result = (*play_obj)->GetInterface(play_obj, SL_IID_BUFFERQUEUE, &play_buffer_queue);
    assert(SL_RESULT_SUCCESS == result);
    result = (*play_obj)->GetInterface(play_obj, SL_IID_EFFECTSEND, &play_effect_send);
    assert(SL_RESULT_SUCCESS == result);
    result = (*play_obj)->GetInterface(play_obj, SL_IID_VOLUME, &play_volume);
    assert(SL_RESULT_SUCCESS == result);
}

ReceiveRTPSession::~ReceiveRTPSession()
{
    /* Destory Speex decoder Bits. */
    speex_bits_destroy(&decoder_bits);
    /* Destory Speex decoder. */
    speex_decoder_destroy(decoder_state);

    /* Release memory to each play buffer. */
    for(play_buffer_index = 0; play_buffer_index < play_buffer_count; play_buffer_index++)
    {
        free(play_buffer[play_buffer_index]);
    }
}

void ReceiveRTPSession::Start()
{
    (*play_interface)->SetPlayState(play_interface, SL_PLAYSTATE_PLAYING);
}

void ReceiveRTPSession::Destroy()
{
    (*play_interface)->SetPlayState(play_interface, SL_PLAYSTATE_STOPPED);
    (*play_obj)->Destroy(play_obj);
    RTPSession::Destroy();
}

void ReceiveRTPSession::OnPollThreadStep()
{
    BeginDataAccess();
    if (GotoFirstSourceWithData())
    {
        do
        {
            RTPPacket *pack;
            while ((pack = GetNextPacket()) != NULL)
            {
                ProcessRTPPacket(*pack);
                DeletePacket(pack);
            }
        } while (GotoNextSourceWithData());
    }
    EndDataAccess();
}

void ReceiveRTPSession::ProcessRTPPacket(const RTPPacket &pack)
{

    play_buffer_index++;
    if(play_buffer_index == play_buffer_count)
    {
        play_buffer_index = 0;
    }
    speex_bits_read_from(&decoder_bits, (char *)pack.GetPayloadData(), pack.GetPayloadLength());
    assert(speex_success_result == speex_decode_int(decoder_state, &decoder_bits, play_buffer[play_buffer_index]));
    (*play_buffer_queue)->Enqueue(play_buffer_queue, play_buffer[play_buffer_index], decoder_frame_size * sizeof(short));
}